(ns stringmerge.core)

(defn is-merge-oneway
  [word p1 p2]
  (let [[p1c & p1s] p1
        [p2c & p2s] p2
        [w & ws] word]
    (println p1c p1s p1)
    (println p2c p2s p2)
    (println w ws word)
    (cond
      (nil? w) true
      (= p1c w) (is-merge-oneway ws p1s p2)
      (= p2c w) (is-merge-oneway ws p1 p2s)
      :else false)))

(defn is-merge
  [word p1 p2]
  (or (is-merge-oneway word p1 p2) (is-merge-oneway word p2 p1)))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

"Pnhh9s''erbgBv!+SAnHs$7TR@CF^lHn\"#4C/O1_;+,&1QyJN:HPnhh9s''erbgBv!+SAnHs$7TR@CCk'AbwYHr5]Z.+A*&#oeV`]4:Pp"
"Pnhh9s''erbgBv!+SAnHs$7TR@CCk'AbwYHr5]Z."
"Pnhh9s''erbgBv!+SAnHs$7TR@CF^lHn\"#4C/O1_;+,&1QyJN:H+A*&#oeV`]4:Pp"

"Bananas from Bahamas"
"Bahas"
"Bananas from am"
