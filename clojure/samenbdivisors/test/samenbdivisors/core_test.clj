(ns samenbdivisors.core-test
  (:require [clojure.test :refer :all]
            [samenbdivisors.core :refer :all]))

(defn do-test [diff nmax ans]
  (is (= (count-pairs-int diff nmax) ans)))

(deftest a-test1
  (println "Basic Tests count-pairs-int")  
  (do-test 1 50 8)
  (do-test 3 100 7)
  (do-test 3 200 18)
  (do-test 6 350 86)
  (do-test 6 1000 214)
  (do-test 7 1500 189)
  (do-test 7 2500 309)
  (do-test 7 3000 366)
  (do-test 9 4000 487)
  (do-test 9 5000 622)
  (do-test 11 5000 567)
)
