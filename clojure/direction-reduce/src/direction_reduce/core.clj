(ns direction-reduce.core)

(def ur ["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"])

(defn dir-reduc-array
  [arr]
  (reduce (fn [acc n]
            (cond
              (empty? acc) [n]
              (and (= (peek acc) "NORTH") (= n "SOUTH")) (vec (drop-last acc))
              (and (= (peek acc) "SOUTH") (= n "NORTH")) (vec (drop-last acc))
              (and (= (peek acc) "EAST") (= n "WEST")) (vec (drop-last acc))
              (and (= (peek acc) "WEST") (= n "EAST")) (vec (drop-last acc))
              :else (conj acc n))) [] ur))

(defn dirReduc
  [arr]
  (let [result (dir-reduc-array arr)]
       (if (empty? result)
         nil
         result)))


