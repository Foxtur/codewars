// Associated Kata: https://www.codewars.com/kata/513e08acc600c94f01000001/train/rust

fn main() {
    println!("{}", rgb(255, 255, 255));
}

fn round_to_nearest(val: i32) -> i32 {
    if val < 0 {
        0
    } else if val > 255 {
        255
    } else {
        val
    }
}

fn rgb(r: i32, g: i32, b: i32) -> String {
    // using https://doc.rust-lang.org/std/fmt/#fillalignment
    let r = round_to_nearest(r);
    let g = round_to_nearest(g);
    let b = round_to_nearest(b);

    format!("{:0>2X}{:0>2X}{:0>2X}", r, g, b)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_returns_ffffff() {
        assert_eq!(rgb(255, 255, 255), "FFFFFF");
    }

    #[test]
    fn it_returns_ffffff_too() {
        assert_eq!(rgb(255, 255, 300), "FFFFFF");
    }

    #[test]
    fn it_returns_000000() {
        assert_eq!(rgb(0, 0, 0), "000000");
    }

    #[test]
    fn it_return_000000_negative_number() {
        assert_eq!(rgb(-255, 0, 0), "000000");
    }

    #[test]
    fn it_returns_9400D3() {
        assert_eq!(rgb(148, 0, 211), "9400D3");
    }
}
