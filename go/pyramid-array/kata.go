package main

import s "github.com/inancgumus/prettyslice"

func Pyramid(n int) [][]int {
  pyramid := make([][]int, n)

  for i := 0; i < n; i++ {
    inner := make([]int, i + 1)
    for j := 0; j <= i; j++ {
      inner[j] = 1
    }
    pyramid[i] = inner
  }
  return pyramid
}

func main() {
  s.Show("pyramid", Pyramid(3))
}

