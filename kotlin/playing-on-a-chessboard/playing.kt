package solution

import kotlin.math.abs

object Suite2 {
    fun game(n: Long): String {
        val fractions = mutableListOf<Fraction>()
        for (i in 1 .. n) {
            for (j in 1 .. n) {
                fractions.add(Fraction(i, i+j))
            }
        }
        return fractions.sortedBy { it.denominator }.reduce { acc, it -> acc + it }.toString()
    }

    fun blub(n: Long): List<Fraction> {
        val fractions = mutableListOf<Fraction>()
        for (i in 1 .. n) {
            for (j in 1 .. n) {
                fractions.add(Fraction(i, i+j))
            }
        }
        return fractions.sortedBy { it.denominator }
    }
}

class Fraction(num: Long, denom: Long) {

    val numerator: Long
    val denominator: Long

    init {
        val g = gcd(abs(num), abs(denom))
        numerator = num / g
        denominator = denom / g
    }

    operator fun plus(that: Fraction) : Fraction {
        return Fraction(
            numerator * that.denominator + that.numerator * this.denominator,
            this.denominator * that.denominator
        ).normalize()
    }

    private fun normalize() : Fraction {
        val g = gcd(abs(numerator), abs(denominator))
        return Fraction(this.numerator / g, this.denominator / g)
    }

    private fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)

    override fun toString() : String {
        return if (denominator > 1)
            "[$numerator, $denominator]"
        else
            "[$numerator]"
    }
}

/*

 for (i in 1 .. n) {
     for (j in 1 .. n) {
     println("$i/${i+j}")
            }
        }

 Even: 2, 8, 18, 32 50 --> sum(n) = n * index
 Odd: 1, 9, 25, 49, 81 --> sum(n) = (n * n) / 2
*/
