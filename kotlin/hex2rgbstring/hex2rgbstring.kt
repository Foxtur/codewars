// you have preloaded
data class RGB(val r: Int, val g: Int, val b: Int)
fun hexStringToRGB(hexString: String): RGB {
    val (r, g, b) = hexString.toLowerCase()
        .substring(1)
        .chunked(2)
        .map { it.toInt(16) }

    return RGB(r,g,b)
}
