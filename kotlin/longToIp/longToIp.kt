// Example Inputs
/*
class LongToIpTest {

    @Test
    fun sampleTest() {
        assertEquals("128.114.17.104", longToIP(2154959208u))
        assertEquals("0.0.0.0", longToIP(0u))
        assertEquals("128.32.10.1", longToIP(2149583361u))
    }
}
*/

fun longToIP(ip: UInt): String {
    val first = ip and 0xFF000000u shr 24
    val second = ip and 0x00FF0000u shr 16
    val third = ip and 0x0000FF00u shr 8
    val fourth = ip and 0x000000FFu
    return "$first.$second.$third.$fourth"
}
