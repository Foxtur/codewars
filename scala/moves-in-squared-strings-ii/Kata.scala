object Kata {
  def rot(s: String): String = {
    return s.split("\\\n")
      .map(s => s.reverse)
      .reverse
      .mkString("\n")
  }

  def selfieAndRot(s: String): String = {
    val rotString = rot(s).split("\\\n")
      .map(s => "." * s.length + s)
      .mkString("\n")
      
    val selfString = s.split("\\\n")
      .map(s => s + "." * s.length)
      .mkString("\n")

    return selfString + "\n" + rotString
  }

  def oper(f: String => String, s: String): String = f(s)

  def main(args: Array[String]) {
    println(oper(selfieAndRot, "abcd\nefgh\nijkl\nmnop"))
  }

}
