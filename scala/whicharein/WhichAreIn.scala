import scala.collection.mutable.ArrayBuffer

object WhichAreIn {
  def inArray(array1: Array[String], array2: Array[String]): Array[String] =
    array1.filter(s => array2.exists(_.contains(s))).distinct.sorted

  def main(args: Array[String]) {
    var a1 = Array("live", "arp", "strong")
    var a2 = Array("lively", "alive", "harp", "sharp", "armstrong")
    inArray(a1, a2).foreach { println }
  }
}
