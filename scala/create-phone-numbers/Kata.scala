object Kata {
  def createPhoneNumber(numbers: Seq[Int]): String = {
    val firstPart = "(" + numbers.take(3).mkString("") + ")"
    val secondPart = numbers.drop(3).take(3).mkString("")
    val thirdPart = numbers.drop(6).mkString("")

    return s"$firstPart $secondPart-$thirdPart"
  }
  
  def main(args: Array[String]) {
    println(s"Result: ${createPhoneNumber(Seq(1,2,3,4,5,6,7,8,9,0))}")
  }
}
