object CamelCase {
  def toCamelCase(s: String): String = {
    s.replaceAll("-", "_")
      .split("_")
      .zipWithIndex.map { case(word, idx) =>
        println(word)
        if(idx == 0)
          word
        else if (word.charAt(0) >= 97)
          word.replaceFirst(word.charAt(0).toString, (word.charAt(0) - 32).toChar.toString)
        else
          word

      }
      .mkString
  }

  def main(args: Array[String]) = println(toCamelCase("You_have_chosen_to_translate_this_kata_For_your_convenience_we_have_provided_the_existing_test_cases_used_for_the_language_that_you_have_already_completed_as_well_as_all_of_the_other_related_fields"))
}
