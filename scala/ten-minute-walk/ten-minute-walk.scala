object Solution {
  def isValidWalk(walk: Seq[Char]): Boolean = {

    if (walk.length != 10)
      return false

    var position = (0, 0)

    for(direction <- walk) {
      direction match {
        case 'n' => position = (position._1 + 1, position._2)
        case 's' => position = (position._1 - 1, position._2)
        case 'w' => position = (position._1, position._2 - 1)
        case 'e' => position = (position._1, position._2 + 1)
      }
    }

    return position == (0, 0)
  }
}
