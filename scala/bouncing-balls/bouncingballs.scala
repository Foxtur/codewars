def bouncingBall(h: Double, bounce: Double, window: Double): Int = {
  if (h <= 0)
    return -1

  if (bounce <= 0 || bounce >= 1)
    return -1

  if (window >= h)
    return -1

  var count = 0
  var cHeight = h
  while(cHeight > window) {
    count += 1
    cHeight = cHeight * bounce
    if(cHeight > window)
      count += 1
  }

  return count
}

println(bouncingBall(3, 0.66, 1.5))
