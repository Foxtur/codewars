object Kata {
  
  def solution(s: String): List[String] = {
    s.grouped(2).map(s => if(s.length < 2) s + "_" else s).toList
  }

}
