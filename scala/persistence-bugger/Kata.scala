object Kata {

  def persistence(n: Int, counter: Int = 0): Int = {
    if (n.toString.length == 1) {
      return counter
    } else {
      return persistence(
        n.toString
          .map(c => c.asDigit)
          .reduce((a, b) => a * b),
          counter + 1
        )
    }
  }
  
  def main(args: Array[String]) =
    println(s"Result: ${persistence(39)}")

}
