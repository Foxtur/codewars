object Kata {

  def beggars(values: List[Int], n: Int): List[Int] = {
    return (0 to n - 1).toList
      .map(i => values.drop(i).grouped(n).map(_.head).sum)
  }

  def main(args: Array[String]) = println(beggars(List(1, 2, 3, 4, 5), 3))
}
