def comp(seq1: Seq[Int], seq2: Seq[Int]): Boolean = {
  seq1 != null && seq2 != null && seq1.map( x => x * x).sorted == seq2.sorted
}

val seq1 = Seq(121, 144, 19, 161, 19, 144, 19, 11)
val seq2 = Seq(11*11, 121*121, 144*144, 19*19, 161*161, 19*19, 144*144, 19*19)

println(comp(seq1, seq2))
